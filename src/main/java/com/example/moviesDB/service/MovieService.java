package com.example.moviesDB.service;

import com.example.moviesDB.model.Movie;
import com.example.moviesDB.repo.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MovieService {
    private final MovieRepository movieRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public Movie addMovie(Movie movie) {
        return movieRepository.save(movie);
    }

    public List<Movie> findFavoriteMovies() {
        return movieRepository.findAll();
    }

    public Movie updateMovie(Movie Movie) {
        return movieRepository.save(Movie);
    }

    public void deleteMovie(Long id){
        movieRepository.deleteMovieById(id);
    }
}
