package com.example.moviesDB.ressource;

import com.example.moviesDB.model.Movie;
import com.example.moviesDB.repo.MovieRepository;
import com.example.moviesDB.service.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MovieRessource {
    private MovieService movieService;
    private MovieRepository movieRepository;

    public void MovieResource(
            MovieService movieService,
            MovieRepository movieRepository
    ) {
        this.movieService = movieService;
        this.movieRepository = movieRepository;
    }

    @GetMapping("/favorites")
    public ResponseEntity<List<Movie>> getAllMovies () {
        List<Movie> movies = this.movieService.findFavoriteMovies();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PostMapping("/favorites")
    public void addMovieToFavorites(@RequestBody List<Movie> addMovieToFavorites) {
        // return movieRepository.(newEmployee);
    }
}
